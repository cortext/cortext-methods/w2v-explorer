from sklearn import manifold
import seaborn as sb
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.mlab import griddata
import math
import matplotlib
import random
sb.set_style(style='white')

def manifoldreduct(vics,method='SE', metric='euclidean',perplexity=30):  
	n_components=2
	n_neighbors=10
	if method=='ISOMAP':
		method = manifold.Isomap(n_neighbors, n_components)
	if method=='MDS':
		method = manifold.MDS(n_components, max_iter=100, n_init=1)
	if method=='TSNE':
		method = manifold.TSNE(n_components=n_components, random_state=0, metric=metric,perplexity=perplexity,method='exact')#init='pca'
	if method=='SE':
		method = manifold.SpectralEmbedding(n_components=n_components,n_neighbors=n_neighbors)
	reduced_vecs = method.fit_transform(vics)
	return reduced_vecs

def plotit(reduced_vecs,words,counter,neighdict={},typex='point',name='',moyenne={},poids={},show=False):
	hexbin=True
	neighdict_inv = {}
	neighdictlist=neighdict.keys()
	for x,voisins in neighdict.iteritems():
		for y in voisins:
			#print 'y',y
			neighdict_inv.setdefault(y,[]).append(x)
	print " len(neighdict)", len(neighdict)
	print len(reduced_vecs)
	#print "neighdict",neighdict
	current_palette = sb.color_palette("Set2", len(neighdict))
	size=np.log(len(reduced_vecs))*2
	fig=plt.figure(0,figsize=(size,size))
	for i,w in enumerate(words):
		if 1:
			if 'pseudo_' in w:
				plt.text(reduced_vecs[i,0], reduced_vecs[i,1], w,fontsize=10,color='b')
				plt.plot(reduced_vecs[i,0], reduced_vecs[i,1], marker='o', color='b', markersize=np.log(counter.get(w,400)),alpha=.5)


			else:
				if w in neighdict_inv:
					#print" w,current_palette[neighdictlist.index(neighdict_inv[w][0])]",w,neighdictlist.index(neighdict_inv[w][0])
					plt.plot(reduced_vecs[i,0], reduced_vecs[i,1], marker='o', color=current_palette[neighdictlist.index(neighdict_inv[w][0])], markersize=3,alpha=.4)
					plt.text(reduced_vecs[i,0], reduced_vecs[i,1], w,fontsize=4)
				else:
					plt.plot(reduced_vecs[i,0], reduced_vecs[i,1], marker='o', color='grey', markersize=2,alpha=.7)
					if show:
						plt.text(reduced_vecs[i,0], reduced_vecs[i,1], w,fontsize=4)
		# else:
		# 	if w in neighdict_inv:
		# 		idx=neighdict.keys().index(neighdict_inv[w][0])
		# 		color = current_palette[idx]
		# 		if w in neighdict.keys():
		# 			idx=neighdict.keys().index(w)
		# 			color = current_palette[idx]
		# 			plt.plot(reduced_vecs[i,0], reduced_vecs[i,1], marker='D', color=color, markersize=np.log(counter.get(w,10)),alpha=.5)
		# 			plt.text(reduced_vecs[i,0], reduced_vecs[i,1], w,fontsize=8,color=color)
		# 		else:
		# 			plt.plot(reduced_vecs[i,0], reduced_vecs[i,1], marker='o', color=color, markersize=np.log(counter.get(w,10)),alpha=.5)
		# 			plt.text(reduced_vecs[i,0], reduced_vecs[i,1], w,fontsize=4,color=color)
		#
		#
		# 	else:
		# 		#plt.plot(reduced_vecs[i,0], reduced_vecs[i,1], marker='o', color='g', markersize=0.1)
		# 		if 'pseudo_' in w:
		# 			plt.text(reduced_vecs[i,0], reduced_vecs[i,1], w,fontsize=10,color='b')
		# 			plt.plot(reduced_vecs[i,0], reduced_vecs[i,1], marker='o', color='b', markersize=np.log(counter.get(w,400)),alpha=.5)
		# 		else:
		# 			plt.text(reduced_vecs[i,0], reduced_vecs[i,1], w,fontsize=2)
	#print np.amin(reduced_vecs, axis=1)
	if len(moyenne)>0:
		#on prepare le dictionnaire des positions des mots
		posdict={}
		for i,w in enumerate(words):
			posdict[w]=np.array([reduced_vecs[i,0],reduced_vecs[i,1]])
		
		if typex=='point':
			years = moyenne.keys()
			years.sort()
			colmem='j'
			for yi,year in enumerate(years):
			
				col=current_palette[neighdictlist.index(str(year).split('_*')[0])]
				counterwords=moyenne[year]
				moyennew=[]
				ponderation=[]
				for w,tf in counterwords.iteritems():
					moyennew.append(posdict[w].reshape((1,2)))
					ponderation.append(tf*poids.get(w,1.))#actually it should always be one as idf was already included in vector calculation before
				vecs = np.concatenate(moyennew)
				infered=np.average(vecs,axis=0, weights=ponderation)
				plt.plot(infered[0], infered[1], marker='o', color=col, markersize=5,alpha=.5 )
				plt.text(infered[0], infered[1],  str(year),fontsize=6)
			
				if col==colmem:
					plt.plot([infered[0], mem[0]], [infered[1], mem[1]], color=col,linestyle ='-',linewidth=.5,alpha=.3)
				mem=(infered[0],infered[1])
				colmem=col
			hexbin=True
		elif typex=='density':
			#years = moyenne.keys()
			#years.sort()
			#colmem='j'
			#for yi,year in enumerate(years):
			#hexbin=False
			#hexbin=True
			if 1:
				col=current_palette[0]
				counterwords=moyenne
				moyennew=[]
				ponderation=[]
				x,y,z=[],[],[]
				for w,tf in counterwords.iteritems():
					infered = posdict[w]
					#print 'w,infered ',w,infered 
					#moyennew.append(infered)
					#ponderation.append(tf*poids.get(w,1.))#actually it should always be one as idf was already included in vector calculation before
					plt.plot(infered[0], infered[1], marker='o', color=col, markersize=3,alpha=.3 )
					#plt.text(infered[0], infered[1],  str(w),fontsize=6)
					if hexbin:
						for i in range(int(tf*20*poids.get(w,1.))):
							x.append(infered[0])
							y.append(infered[1])
					else:
						x.append(infered[0])
						y.append(infered[1])
						z.append(tf*poids.get(w,1.))
						#plt.text(infered[0],infered[1],str(tf*poids.get(w,1.)))
						#z.append(random.randint(1, 10))
						#print w,'z',tf*poids.get(w,1.)
				if not hexbin:
					for w,infered in posdict.iteritems():
						if not w in counterwords:
							x.append(infered[0])
							y.append(infered[1])
							z.append(0)
				mins= np.amin(reduced_vecs, axis=0)
				maxs= np.amax(reduced_vecs, axis=0)
				
				if hexbin:
					nx=int(math.sqrt(len(words)))/4
					
					plt.hexbin(x, y, cmap=plt.cm.Blues,gridsize=int(nx),alpha=.7,extent=[mins[0], maxs[0],mins[1], maxs[1]],mincnt=1)
					cb = plt.colorbar()
					ax = plt.gca()
					#plt.clim(0,max(z))
					#fig.patch.set_facecolor('white')
					cb.set_label('counts')
				else:
					nx=int(math.sqrt(len(words)))
					nx=300/2
					widthx=maxs[0] -mins[0]
					widthy=maxs[1] -mins[1]
					xi = np.linspace(mins[0]-widthx/7, maxs[0]+widthx/7, (nx+1)/2)
					#print 'xi',xi
					yi = np.linspace(mins[1]-widthy/7, maxs[1]+widthy/7, (nx+1)/2)
					#xi, yi = np.meshgrid(xi, yi)
					
					# x = np.r_[x,mins[0],mins[0],maxs[0],maxs[0]]
					# y = np.r_[y,mins[1],maxs[1],mins[1],maxs[1]]
					# z = np.r_[z,0,0,0,0]
					
					#print xi
					# Interpolate using "delaunay" triangularization
					#zi = griddata(x,y,z,xi,yi,interp='linear')
					from scipy.interpolate import griddata
					zi = griddata((x, y), z, (xi[None,:], yi[:,None]), method='linear')

					
					#sns.heatmap(df,robust=True,cmap="inferno",vmin=0)
					#fig=plt.figure()
					ax = plt.gca()


					#ax.tick_params(direction='inout', pad=50,width=50)
					#mpl.rcParams['ytick.major.size'] = 5
					#mpl.rcParams['ytick.major.width'] = 2
					#mpl.rcParams['xtick.minor.size'] = 0
					#mpl.rcParams['xtick.minor.width'] = 0

					#plt.pcolor(xi,yi,zi,cmap="inferno_r")
					#plt.pcolormesh(xi,yi,zi,cmap="inferno_r")
					#plt.scatter(x,y,c=z,cmap="inferno_r")
					#CS = plt.contour(xi,yi,zi,20,linewidths=0.5,colors='k')
					#c_blue= matplotlib.colors.colorConverter.to_rgba('blue',alpha=.9)
					#c_red = matplotlib.colors.colorConverter.to_rgba('red',alpha=.9)
					#c_white_trans = matplotlib.colors.colorConverter.to_rgba('white',alpha = 0.0)
					#cmap_rb = matplotlib.colors.LinearSegmentedColormap.from_list('rb_cmap',[c_blue,c_white_trans,c_red],512)
					#cmap_rb = matplotlib.colors.LinearSegmentedColormap.from_list('rb_cmap',bwr,512)
					# CS2 = plt.contourf(xi,yi,zi,20,cmap='bwr')
					# CS2 = plt.contourf(xi,yi,zi,20,cmap='bwr')
					#print zi
					#zi=zi+0.01
						
					zibis=np.nan_to_num(zi)
					#CS2 = plt.contourf(xi,yi,zi,201,cmap='bwr',vmin=-np.amax(zibis), vmax=np.amax(zibis))
					#CS2 = plt.contour(xi,yi,zi,20,cmap='bwr',vmin=-np.amax(zibis), vmax=np.amax(zibis))
					#CS2 = plt.contourf(xi,yi,zi,200,cmap='bwr',vmin=-np.amax(zibis), vmax=np.amax(zibis))
					CS2 = plt.contourf(xi,yi,zi,200,cmap='bwr',vmin=-0.076, vmax=0.076)
					#CS2 = plt.contourf(xi,yi,zi,20,cmap='bwr',vmin=-max(z), vmax=max(z))

					
					
	
					#fig = plt.figure()
					#cmap.set_bad('w',0)
					#plt.patch.set_facecolor('white')
					#plt.patch.set_alpha(1)
					print "max(z),min(z)",max(z),min(z)
					print '-np.amax(zibis)',-np.amax(zibis)
					
					plt.colorbar(shrink=.6)
					#

					
					
					#print "max(zi)",np.amax(zibis)
					#plt.clim(-np.amax(zi),np.amax(zi))
					
				# if col==colmem:
				# 	plt.plot([infered[0], mem[0]], [infered[1], mem[1]], color=col,linestyle ='-',linewidth=.5,alpha=.3)
				# mem=(infered[0],infered[1])
				# colmem=col
			# ocpos=[]
			# for counterwords in moyenne:
			# 	moyennew=[]
			# 	ponderation=[]
			# 	for w,tf in counterwords.iteritems():
			# 		moyennew.append(posdict[w].reshape((1,2)))
			# 		ponderation.append(tf*poids.get(w,1.))
			# 		vecs = np.concatenate(moyennew)
			# 		infered=np.average(vecs,axis=0, weights=ponderation)
			# 		ocpos.append(infered)
			# x,y=[],[]
			# for oc in ocpos:
			# 	x.append(oc[0])
			# 	y.append(oc[1])
			# 	#plt.plot(oc[0], oc[1], marker='.',color='b',  markersize=5,alpha=.5 )
			# mins= np.amin(reduced_vecs, axis=0)
			# maxs= np.amax(reduced_vecs, axis=0)
			#
			# plt.hexbin(x, y, cmap=plt.cm.Blues,gridsize=25,alpha=.5,extent=[mins[0], maxs[0],mins[1], maxs[1]])
			#
			# cb = plt.colorbar()
			# cb.set_label('counts')
				
				
				
		
			
	mins= np.amin(reduced_vecs, axis=0)
	maxs= np.amax(reduced_vecs, axis=0)
	plt.xlim(mins[0],maxs[0])
	plt.xlim(mins[1],maxs[1])
	#plt.ylim(mins[1],maxs[1])
	plt.axis('off')
	#plt.tight_layout()
	#plt.title(name)
	#plt.legend()
	print 'emap'+name+'_'.join(map(lambda x:str(x),neighdict.keys()))+'.pdf', 'produced'
	if typex=='density':
		plt.savefig('density/emap'+name+'_'.join(map(lambda x:str(x),neighdict.keys()))+'_hex_'+str(hexbin)+'.pdf')
	else:
		plt.savefig('emap'+name+'_'.join(map(lambda x:str(x),neighdict.keys()))+'_hex_'+str(hexbin)+'.pdf')
	plt.close('all')
	
import seaborn
seaborn.set_style(style='white')
import pandas
#print deltamats['child']
target='freedom'

def buildvecx(deltamats,normalized=False):
#print deltamats['men'].keys()
	if normalized:
		timesteps = deltamats.keys()
		timesteps=[1789]+timesteps
		timesteps.sort()
	x,y,z=[],[],[]
	for fr in deltamats:
		for to in deltamats[fr]:
			x.append(fr)
			y.append(to)
			if normalized:
				#print fr,to
				if fr==to:
					z.append(0)
				else:
					fridx = timesteps.index(fr)
					toidx = timesteps.index(to)
					
					frrange = range(timesteps[fridx-1]+1,timesteps[fridx]+1)
					torange = range(timesteps[toidx-1]+1,timesteps[toidx]+1)
					#if target=='allofthem':
					#	print fr,to,frrange,torange
					norm = 0
					ii=0
					for f in frrange:
						for t in torange:
							try:
								norm+=deltamats['allofthem'][f][t]
								ii+=1
							except:
								pass
					norm=norm/float(ii)
					#norm=deltamats['allofthem'][fr][to]
					z.append(deltamats[fr][to]-norm)
			else:
				z.append(deltamats[fr][to])
	return x,y,z

def translate_years(x,ratio=0):
	print 'ratio',ratio
	years= list(set(x))
	dict_pos_years={}
	dict_label_years={}
	years.sort()
	#print years
	if not 1789 in years:
		years=[1789]+years
	for i,y in enumerate(years[:-1]):
		#new_years.append((y+1+years[i+1])/2)
		#new_years_label.append(str(y+1) + '-' + str(years[i+1]))
		dict_pos_years[years[i+1]]=(y+1+years[i+1])/2
		dict_label_years[(y+1+years[i+1])/2]=str(y+1) + '-' + str(years[i+1])
	if ratio>0:#on decale le debut des intervalles
		dict_pos_years_ratio={}
		dict_label_years_ratio={}
		final_years = dict_pos_years.keys()
		final_years.sort()
		# print 'dict_pos_years',dict_pos_years
		# print 'dict_label_years',dict_label_years
		# print 'final_years',final_years
		for i,final_year in enumerate(final_years):
			# print "\n*i,final_year",i,final_year
			if i==0:
				dict_pos_years_ratio[final_year]=dict_pos_years[final_year]
				dict_label_years_ratio[dict_pos_years[final_year]]=dict_label_years[dict_pos_years[final_year]]
			else:
				# print "dict_label_years_ratio",dict_label_years_ratio
				startingprev=int(dict_label_years_ratio[dict_pos_years_ratio[final_years[i-1]]].split('-')[0])
				endingprev=int(dict_label_years_ratio[dict_pos_years_ratio[final_years[i-1]]].split('-')[1])
				# print 'voila',startingprev,'-',endingprev
				delta=ratio*(endingprev-startingprev)
				# print 'delta',delta
				newfirs=int(endingprev - delta )
				newlast=final_year
				dict_pos_years_ratio[final_year]=int((newfirs + newlast)/2)
				dict_label_years_ratio[int((newfirs + newlast)/2)]= str(int(endingprev - delta )) + '-' + str(final_year)
				# print "dict_label_years_ratiores",dict_label_years_ratio
				# print 'dict_pos_years_ratiores',dict_pos_years_ratio
		return dict_pos_years_ratio,dict_label_years_ratio		
	else:
		return dict_pos_years,dict_label_years

		
def plot_word(deltamats,w,normalized=False,name='',ratio=0):
	x,y,z=buildvecx(deltamats,normalized=False)
	
	dict_pos_years,new_years_label=translate_years(x,ratio=ratio)
	xbis,ybis=[],[]
	for h in x:
		xbis.append(dict_pos_years[h])
	for h in y:
		ybis.append(dict_pos_years[h])
	x=xbis
	y=ybis
	xmin=min(x)
	xmax=max(x)
	print "xmin,xmax",xmin,xmax
	xold=x[:]
	for h in range(xmin,xmax):
		if normalized:
			x.append(h)
			y.append(h)
			z.append(0)
		else:
			x.append(h)
			y.append(h)
			z.append(1)
	print max(z)
	x=np.array(x)
	y=np.array(y)
	z=np.array(z)
	nx=xmax-xmin
	xi = np.linspace(xmin, xmax, (nx+1)/2)
	#print 'xi',xi
	yi = np.linspace(xmin, xmax, (nx+1)/2)
	xi = np.linspace(xmin, xmax, (nx+1))
	yi = np.linspace(xmin, xmax, (nx+1))
	xi, yi = np.meshgrid(xi, yi)
	#print xi
	# Interpolate using "delaunay" triangularization 
	zi = griddata(x,y,z,xi,yi,interp='linear')
	#sns.heatmap(df,robust=True,cmap="inferno",vmin=0)
	fig=plt.figure()
	ax = plt.gca()
	

	#ax.tick_params(direction='inout', pad=50,width=50)
	#mpl.rcParams['ytick.major.size'] = 5
	#mpl.rcParams['ytick.major.width'] = 2
	#mpl.rcParams['xtick.minor.size'] = 0
	#mpl.rcParams['xtick.minor.width'] = 0

	#plt.pcolor(xi,yi,zi,cmap="inferno_r")
	#plt.pcolormesh(xi,yi,zi,cmap="inferno_r")
	#plt.scatter(x,y,c=z,cmap="inferno_r")
	# CS = plt.contour(xi,yi,zi,20,linewidths=0.5,colors='k')
	# CS2 = plt.contourf(xi,yi,zi,20,cmap=plt.cm.jet_r)
	# plt.clim(0.65,1)
	
	
	if normalized:
		#CS = plt.contour(xi,yi,zi,20,linewidths=0.5,colors='k',vmin=-max(max(z),-min(z)),vmax=max(max(z),-min(z)))
		CS2 = plt.contourf(xi,yi,zi,200,cmap='RdBu_r',vmin=-max(max(z),-min(z)),vmax=max(max(z),-min(z)))
#		plt.scatter(x,y,c=z,cmap='RdBu_r')
		
		plt.clim(-.2,.2)
		#plt.clim(-.5,.5)
	else:
		print 'max(z),min(z)',max(z),min(z)
		#CS = plt.contour(xi,yi,zi,20,linewidths=0.5,colors='k')
		CS2 = plt.contourf(xi,yi,zi,20,cmap=plt.cm.jet_r,vmax=1)
		#plt.scatter(x,y,c=z,cmap=plt.cm.jet_r)
		
		plt.clim(0.55,1)
	
	
	plt.colorbar()
	#plt.clim(0.,5)
	
	#plt.xticks(xold)
	# plt.xticks(new_years_label.keys(), new_years_label.values(), rotation='vertical',fontsize=6)
	# plt.xticks(rotation=90)
	plt.xticks(range(int(xmin/10)*10,10+int(xmax/10)*10,10))
	plt.xticks(rotation=90)
	plt.yticks(range(int(xmin/10)*10,10+int(xmax/10)*10,10))
	plt.axis([xmin, xmax, xmin, xmax])
	plt.axis([1790, 2016, 1790, 2016])


	plt.title(w)
	#plt.show()
	print 'saving file','countours/examples/contour'+w+'_ratio_'+name+'.pdf'
	plt.savefig('countours/examples/contour'+w+'_ratio_'+name+str(normalized)+'.pdf')
	return xi,yi,zi

# for t in pseudowords[:-1]:
#	 if 1:
#		 print t,
#		 plot_word(t,normalized=False)
#	 else:
#		 print t
#		 print 'triangulation error ?'
